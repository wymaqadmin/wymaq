<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//------------------ABM CATEGORIAS-------------------------------
Route::post('/categorias', 'CategoriasController@postCategorias');
Route::get('/categorias', 'CategoriasController@getCategorias');
Route::get('/categorias/{id}', 'CategoriasController@getCategoriasId');
Route::put('/categorias/{id}', 'CategoriasController@putCategorias');
Route::delete('/categorias/{id}', 'CategoriasController@deleteCategorias');

//------------------ABM SERVICIOS-------------------------------
Route::post('/servicios', 'ServiciosController@postServicios');
Route::get('/servicios', 'ServiciosController@getServicios');
Route::get('/servicios/{id}', 'ServiciosController@getServiciosId');
Route::get('/servicios/categoria/{id_categoria}', 'ServiciosController@getServiciosIdCategoria');
Route::put('/servicios/{id}', 'ServiciosController@putServicios');
Route::delete('/servicios/{id}', 'ServiciosController@deleteServicios');