<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Usuarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       // Creacion de la tabla usuarios y todo lo relacionado con ellos
        // Las FK siempre tienen que ser unsigned porque los 'increments' son unsigned.
        Schema::create('usuario_estados', function (Blueprint $table) {
            $table->increments('id');
            $table->string('denominacion', 45);
        });

        Schema::create('perfiles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('denominacion', 45);            
        });

        Schema::create('usuarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->unique();
            $table->string('password', 255);
            $table->string('nombre', 255);
            $table->string('apellido', 255);
            $table->string('empresa', 255)->nullable();
            $table->string('telefono', 255)->nullable();
            $table->integer('usuario_estado_id')->unsigned()->default(1);
            $table->foreign('usuario_estado_id')->references('id')->on('usuario_estados')->onDelete('cascade');        
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('usuarios_perfiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('usuario_id')->unsigned();
            $table->integer('perfilusuario_id')->unsigned();
            $table->foreign('usuario_id')->references('id')->on('usuarios');
            $table->foreign('perfilusuario_id')->references('id')->on('perfiles')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });




        Schema::create('grupos_tecnicos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('denominacion', 127);
        });

        Schema::create('categorias', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 127);
            $table->integer('order')->nullable();
            $table->string('keywords', 127)->nullable();
            $table->integer('categoria_padre_id')->unsigned()->nullable();
            $table->foreign('categoria_padre_id')->references('id')->on('categorias')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('tipos_productos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 127);
            $table->integer('categoria_id')->unsigned();
            $table->foreign('categoria_id')->references('id')->on('categorias')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('tipos_caracteristicas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('denominacion', 127);
        });

        Schema::create('caracteristicas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 127);
            $table->string('descripcion', 255);
            $table->integer('tipo_caracteristica_id')->unsigned();
            $table->foreign('tipo_caracteristica_id')->references('id')->on('tipos_caracteristicas')->onDelete('cascade');
           
        });

        Schema::create('tipos_de_productos_caracteristicas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('valor', 127);
            $table->integer('tipo_de_producto_id')->unsigned();
            $table->integer('caracteristica_id')->unsigned();
            $table->foreign('tipo_de_producto_id')->references('id')->on('tipos_productos')->onDelete('cascade');
            $table->foreign('caracteristica_id')->references('id')->on('caracteristicas')->onDelete('cascade');
            
        });

        Schema::create('servicios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('categoria_id')->unsigned();
            $table->foreign('categoria_id')->references('id')->on('categorias')->onDelete('cascade');
            
        });

        Schema::create('categorias_servicios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('categoria_id')->unsigned();
            $table->integer('servicio_id')->unsigned();
            $table->foreign('categoria_id')->references('id')->on('categorias')->onDelete('cascade');
            $table->foreign('servicio_id')->references('id')->on('servicios')->onDelete('cascade');
        });

        Schema::create('categorias_caracteristicas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('categoria_id')->unsigned();
            $table->integer('caracteristica_id')->unsigned();
            $table->foreign('categoria_id')->references('id')->on('categorias')->onDelete('cascade');
            $table->foreign('caracteristica_id')->references('id')->on('caracteristicas')->onDelete('cascade');
        });

        Schema::create('aspectos_tecnicos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('denominacion', 127);
            $table->string('descripcion', 511);
            $table->integer('grupo_tecnico_id')->unsigned();
            $table->foreign('grupo_tecnico_id')->references('id')->on('grupos_tecnicos')->onDelete('cascade');
        });

        Schema::create('categorias_aspectos_tecnicos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('categoria_id')->unsigned();
            $table->integer('aspecto_tecnico_id')->unsigned();
            $table->foreign('categoria_id')->references('id')->on('categorias')->onDelete('cascade');
            $table->foreign('aspecto_tecnico_id')->references('id')->on('aspectos_tecnicos')->onDelete('cascade');
        });

        Schema::create('archivos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('entidad', 16);
            $table->integer('entidad_id')->unsigned();
            $table->integer('usuario_id')->unsigned();
            $table->string('comentario', 255);
            $table->string('path_relativo', 511);
            $table->string('nombre_original', 255);
            $table->integer('peso');
            $table->string('extension', 7);
            $table->string('mime_type', 255);
            $table->integer('orden');
            $table->string('categoria', 255);
            $table->foreign('usuario_id')->references('id')->on('usuarios')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios_perfiles');
        Schema::dropIfExists('usuarios');
        Schema::dropIfExists('perfiles');
        Schema::dropIfExists('usuario_estados');

        Schema::dropIfExists('grupos_tecnicos');
        Schema::dropIfExists('categorias');
        Schema::dropIfExists('tipos_productos');
        Schema::dropIfExists('tipos_caracteristicas');
        Schema::dropIfExists('caracteristicas');
        Schema::dropIfExists('tipos_de_productos_caracteristicas');
        Schema::dropIfExists('servicios');
        Schema::dropIfExists('categorias_servicios');
        Schema::dropIfExists('categorias_caracteristicas');
        Schema::dropIfExists('aspectos_tecnicos');
        Schema::dropIfExists('categorias_aspectos_tecnicos');
        Schema::dropIfExists('archivos');
    }
}
