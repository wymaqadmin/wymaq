<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Servicio extends Model
{
    public static function createServicio($data){
        $response=array();
        $id_servicio=DB::table('servicios')->insertGetId([
            'categoria_id' =>$data->categoria_id,
        ]);
        if($id_servicio>0){
            $id_cat_serv=DB::table('categorias_servicios')->insertGetId([
                'categoria_id' =>$data->categoria_id,
                'servicio_id' =>$id_servicio,
            ]);
            if($id_cat_serv>0){
                $response['status_code']=200;
                $response['message']='SERVICIO REGISTRADO CON EXITO';
                return $response;
            }else{
                $response['status_code']=500;
                $response['message']='ERROR AL ENLAZAR CATEGORIA CON SERVICIO';
                return $response;
            }
            
        }else{
            $response['status_code']=500;
            $response['message']='ERROR AL REGISTRAR SERVICIO';
            return $response;
        }

    }

    public static function listServicios(){
        $response=array();
        $query = DB::table('servicios')->get();
        if(count($query) > 0){
            $response['status_code']=200;
            $response['message']='SERVICIOS ENCONTRADOS CON EXITO';
            $response['Servicios']=$query;
            return $response;
        }else{
            $response['status_code']=404;
            $response['message']='NO HAY SERVICIOS REGISTRADOS';
            return $response;
        }
    }


    public static function listServicioId($id){
        $response=array();
        $query = DB::table('servicios')->where('id', '=', $id)->get();
        if(count($query) > 0){
            $response['status_code']=200;
            $response['message']='SERVICIO ENCONTRADO CON EXITO';
            $response['Servicio']=$query;
            return $response;
        }else{
            $response['status_code']=404;
            $response['message']='SERVICIO NO ENCONTRADO';
            return $response;
        }
    }

    public static function listServicioIdCategoria($id_categoria){
        $response=array();
        $query = DB::table('servicios')->where('categoria_id', '=', $id_categoria)->get();
        if(count($query) > 0){
            $response['status_code']=200;
            $response['message']='SERVICIOS ENCONTRADO CON EXITO';
            $response['Servicio']=$query;
            return $response;
        }else{
            $response['status_code']=404;
            $response['message']='SERVICIOS NO ENCONTRADO';
            return $response;
        }
    }

    public static function updateServicio($data,$id){
        $response=array();
        $updates = DB::table('servicios')
        ->where('id', '=', $id)
        ->update([
            'categoria_id' =>$data->categoria_id,
        ]);
        if($updates>0){
            $update_C_S = DB::table('categorias_servicios')
            ->where('servicio_id', '=', $id)
            ->update([
                'categoria_id' =>$data->categoria_id,
            ]);
            if($update_C_S>0){
                $response['status_code']=200;
                $response['message']='SERVICIO ACTUALIZADO CON EXITO';
                return $response;
            }else{
                $response['status_code']=500;
                $response['message']='ERROR AL ACTUALIZAR ENLACE CATEGORIA CON SERVICIO';
                return $response;
            }
        }else{
            $response['status_code']=500;
            $response['message']='ERROR AL ACTUALIZAR SERVICIO';
            return $response;
        }
        
    }

    public static function eliminarServicio($id){
        $response=array();
        $delete = DB::table('servicios')
        ->where('id', '=', $id)
        ->delete();
        if($delete>0){
            $response['status_code']=200;
            $response['message']='SERVICIO ELIMINADO CON EXITO';
            return $response;
        }else{
            $response['status_code']=500;
            $response['message']='ERROR AL ELIMINAR SERVICIO';
            return $response;
        }
        
    }
}
