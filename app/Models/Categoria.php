<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Categoria extends Model
{

    public static function createCategoria($data){
        $response=array();
        $query = DB::table('categorias')
        ->select('nombre')
        ->where('nombre', '=', $data->nombre)
        ->get();
        
        if(count($query) > 0){
            $response['status_code']=500;
            $response['message']='YA EXISTE UNA CATEGORIA CON ESE NOMBRE';
            return $response;
        }else{
            $ret=DB::table('categorias')->insert([
                'nombre' => $data->nombre,
                'order' => $data->order,
                'keywords' => $data->keywords,
                'categoria_padre_id' =>$data->categoria_padre_id,
                "created_at" =>  \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now()
            ]);
            if($ret==true){
                $response['status_code']=200;
                $response['message']='CATEGORIAS REGISTRADA CON EXITO';
                return $response;
            }else{
                $response['status_code']=500;
                $response['message']='ERROR AL REGISTRAR CATEGORIA';
                return $response;
            }
        }
    }

    public static function listCategorias(){
        $response=array();
        $query = DB::table('categorias')->get();
        if(count($query) > 0){
            $response['status_code']=200;
            $response['message']='CATEGORIAS ENCONTRADAS CON EXITO';
            $response['Categorias']=$query;
            return $response;
        }else{
            $response['status_code']=404;
            $response['message']='NO HAY CATEGORIAS REGISTRADAS';
            return $response;
        }
    }


    public static function listCategoriaId($id){
        $response=array();
        $query = DB::table('categorias')->where('id', '=', $id)->get();
        if(count($query) > 0){
            $response['status_code']=200;
            $response['message']='CATEGORIA ENCONTRADA CON EXITO';
            $response['Categoria']=$query;
            return $response;
        }else{
            $response['status_code']=404;
            $response['message']='CATEGORIA NO ENCONTRADA';
            return $response;
        }
    }

    public static function updateCategoria($data,$id){
        $response=array();
        $updates = DB::table('categorias')
        ->where('id', '=', $id)
        ->update([
            'nombre' => $data->nombre,
            'order' => $data->order,
            'keywords' => $data->keywords,
            'categoria_padre_id' =>$data->categoria_padre_id,
            "updated_at" => \Carbon\Carbon::now()
        ]);
        if($updates>0){
            $response['status_code']=200;
            $response['message']='CATEGORIA ACTUALIZADA CON EXITO';
            return $response;
        }else{
            $response['status_code']=500;
            $response['message']='ERROR AL ACTUALIZAR CATEGORIA';
            return $response;
        }
        
    }

    public static function eliminarCategoria($id){
        $response=array();
        $delete = DB::table('categorias')
        ->where('id', '=', $id)
        ->delete();
        if($delete>0){
            $response['status_code']=200;
            $response['message']='CATEGORIA ELIMINADA CON EXITO';
            return $response;
        }else{
            $response['status_code']=500;
            $response['message']='ERROR AL ELIMINAR CATEGORIA';
            return $response;
        }
    }
}
