<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Servicio;
use App\Models\Categoria;

class ServiciosController extends Controller
{
    public static function postServicios(Request $request){
        $response=array();
        $encontrado=Categoria::listCategoriaId($request->categoria_id);
        if($encontrado['status_code']==200){
            return response()->json(Servicio::createServicio($request));
        }else{
            $response['status_code']=404;
            $response['message']='NO EXISTE EL ID DE CATEGORIA QUE INTENTA INGRESAR';
            return $response;
        }
    }
    
    public static function getServicios(){
        return response()->json(Servicio::listServicios());
    }

    public static function getServiciosId($id){
        return response()->json(Servicio::listServicioId($id));
    }

    public static function getServiciosIdCategoria($id_categoria){
        return response()->json(Servicio::listServicioIdCategoria($id_categoria));
    }

    public static function putServicios(Request $request,$id){
        return response()->json(Servicio::updateServicio($request,$id));
    }

    public static function deleteServicios($id){
        return response()->json(Servicio::eliminarServicio($id));
    }
}
