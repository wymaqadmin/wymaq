<?php

namespace App\Http\Controllers;
use App\Models\Categoria;

use Illuminate\Http\Request;

class CategoriasController extends Controller
{
    public static function postCategorias(Request $request){
        return response()->json(Categoria::createCategoria($request));
    }
    
    public static function getCategorias(){
        return response()->json(Categoria::listCategorias());
    }

    public static function getCategoriasId($id){
        return response()->json(Categoria::listCategoriaId($id));
    }

    public static function putCategorias(Request $request,$id){
        return response()->json(Categoria::updateCategoria($request,$id));
    }

    public static function deleteCategorias($id){
        return response()->json(Categoria::eliminarCategoria($id));
    }
}
